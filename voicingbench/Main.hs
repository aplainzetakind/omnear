module Main where

import Omnear

main :: IO ()
main = print $ voice inversionOverBass (take 8 testChords)
