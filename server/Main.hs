{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
module Main where

import Omnear

import Control.Monad.IO.Class
import Servant
import Network.Wai
import Network.Wai.Handler.Warp
import Test.QuickCheck

type ExerciseAPI = "ex" :> Get '[JSON] Block

exerciseAPI :: Proxy ExerciseAPI
exerciseAPI = Proxy

server :: Server ExerciseAPI
server = liftIO $ generate exercise1

app :: Application
app = serve exerciseAPI server

main :: IO ()
main = run 1111 app
