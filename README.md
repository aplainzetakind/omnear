Install [ghcup](https://github.com/haskell/ghcup) by following the instructions. `ghcup` is a script that can install multiple versions of GHC in an isolated location and lets you switch between them. Make sure that `cabal-install` is also installed if you use the manual method.

Clone this repo. At the root of the project, issue `cabal new-build`. Cabal
will install dependencies and build executables (all isolated). Then you can
run the server by issuing `cabal new-run omnear-server`. The server responds to
GET requests at `localhost:1111/ex`.
