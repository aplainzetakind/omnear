module Omnear.Internal.AStar where

import Data.Maybe ( catMaybes
                  , isJust
                  , fromJust )
import qualified Data.Set as S
import qualified Data.Map as M
import qualified Data.PSQueue as Q
import Data.PSQueue (Binding (..))


aStar :: (Ord a, Show a) => S.Set a -- The set of all nodes.
      -> (a -> a -> Bool) -- Whether the second node is
                          -- accessible from the first.
      -> (a -> Bool) -- Whether a node can be a starting node
      -> (a -> Bool) -- Whether a node can be final
      -> (a -> Double) -- A function measuring individual badness of nodes
      -> (a -> a -> Double) -- A measure of the cost of moving from one node
                            -- to another.
      -> (a -> Double) -- Heuristic badness.
      -> [a] -- The best path.
aStar all next init end bads badt heur | S.null all = []
aStar all next init end bads badt heur
       = let stq = foldr (\x q -> Q.insert x (heur x + bads x, bads x) q)
                                                   Q.empty (S.filter init all)
             go mp closed open
                | end this
                      = reverse . catMaybes . takeWhile isJust
                        . iterate ( fmap snd . ((`M.lookup` mp) =<<) )
                        $ Just this
                | otherwise
                      = go mp' closed' open'
                  where mp'     = foldr (\y p -> M.insertWith min y
                                                    (newhb y, this) p) mp nbs
                        closed' = S.insert this closed
                        open'   = foldr (\y q -> Q.insertWith min y
                                                    (newhb y, newb y) q) q' nbs
                        newhb y = newb y + heur y
                        newb y  = bv + bads y + badt this y
                        nbs     = S.filter (next this) all S.\\ closed
                        (this :-> (_, bv) , q') = fromJust . Q.minView $ open
         in  go M.empty S.empty stq
