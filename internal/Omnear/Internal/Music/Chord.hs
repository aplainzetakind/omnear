module Omnear.Internal.Music.Chord where

import Omnear.Internal.Music.Pitch
import Omnear.Internal.Music.Interval
import Omnear.Internal.Music.Transposable

import Data.Bifunctor

newtype ChordQuality = CQ { getIntervals :: [Interval] } deriving Eq

instance Show ChordQuality where
  show (CQ is) = show is

instance Read ChordQuality where
  readsPrec n = fmap (first CQ) . readsPrec n

majorTriad        :: ChordQuality
majorTriad        = CQ [ I Prf Unison
                       , I Maj Third
                       , I Prf Fifth ]

minorTriad        :: ChordQuality
minorTriad        = CQ [ I Prf Unison
                       , I Min Third
                       , I Prf Fifth ]

diminishedTriad   :: ChordQuality
diminishedTriad   = CQ [ I Prf Unison
                       , I Min Third
                       , I (Dim 1) Fifth ]

augmentedTriad    :: ChordQuality
augmentedTriad    = CQ [ I Prf Unison
                       , I Maj Third
                       , I (Aug 1) Fifth ]

major7th          :: ChordQuality
major7th          = CQ [ I Prf Unison
                       , I Maj Third
                       , I Prf Fifth
                       , I Maj Seventh ]

minor7th          :: ChordQuality
minor7th          = CQ [ I Prf Unison
                       , I Min Third
                       , I Prf Fifth
                       , I Min Seventh ]

dominant7th       :: ChordQuality
dominant7th       = CQ [ I Prf Unison
                       , I Maj Third
                       , I Prf Fifth
                       , I Min Seventh ]

halfDiminished7th :: ChordQuality
halfDiminished7th = CQ [ I Prf Unison
                       , I Min Third
                       , I (Dim 1) Fifth
                       , I Min Seventh ]

diminished7th     :: ChordQuality
diminished7th     = CQ [ I Prf Unison
                       , I Min Third
                       , I (Dim 1) Fifth
                       , I (Dim 1) Seventh ]

minormajor7th     :: ChordQuality
minormajor7th     = CQ [ I Prf Unison
                       , I Min Third
                       , I Prf Fifth
                       , I Maj Seventh ]

major6th          :: ChordQuality
major6th          = CQ [ I Prf Unison
                       , I Maj Third
                       , I Prf Fifth
                       , I Maj Sixth ]

minor6th          :: ChordQuality
minor6th          = CQ [ I Prf Unison
                       , I Min Third
                       , I Prf Fifth
                       , I Maj Sixth ]

data Chord = Chord { getRoot    :: PitchClass,
                     getQuality :: ChordQuality } deriving Eq

instance Show Chord where
  show (Chord pc cq) = show pc ++ show cq

instance Read Chord where
  readsPrec _ cs = reads cs >>= \(pc,str) -> map (\(cq,str')
                                            -> (Chord pc cq, str')) (reads str)

pitchClassesOfChord :: Chord -> [PitchClass]
pitchClassesOfChord (Chord k q) = map (`transposeUp` k) . getIntervals $ q

type Degree = Interval

data ChordFunction = ChordFunction Degree ChordQuality

setFunctionInKey :: PitchClass -> ChordFunction -> Chord
setFunctionInKey p (ChordFunction d q) = Chord
                    (transposeUp d p) q

newtype Cadence = Cadence { getFunctions :: [ChordFunction] }

newtype Progression = Prog { getChords :: [Chord] } deriving Eq

instance Show Progression where
  show (Prog cs) = show cs

instance Read Progression where
  readsPrec n = fmap (first Prog) . readsPrec n

major_I_IV_V_I_triad :: Cadence
major_I_IV_V_I_triad
         = Cadence [ ChordFunction (I Prf Unison) majorTriad
                   , ChordFunction (I Prf Fourth) majorTriad
                   , ChordFunction (I Prf Fifth) majorTriad
                   , ChordFunction (I Prf Unison) majorTriad ]

major_I_IV_V_I_seventh :: Cadence
major_I_IV_V_I_seventh
           = Cadence [ ChordFunction (I Prf Unison) major7th
                     , ChordFunction (I Prf Fourth) major7th
                     , ChordFunction (I Prf Fifth) dominant7th
                     , ChordFunction (I Prf Unison) major7th ]

major_ii_V_I_seventh :: Cadence
major_ii_V_I_seventh
           = Cadence [ ChordFunction (I Maj Second) minor7th
                     , ChordFunction (I Prf Fifth) dominant7th
                     , ChordFunction (I Prf Unison) major7th ]

allDegrees = [ I Prf Unison
             , I Min Second
             , I Maj Second
             , I Min Third
             , I Maj Third
             , I Prf Fourth
             , I (Dim 1) Fifth
             , I Prf Fifth
             , I Min Sixth
             , I Maj Sixth
             , I Min Seventh
             , I Maj Seventh ]

setCadenceInKey :: PitchClass -> Cadence -> Progression
setCadenceInKey k = Prog . fmap (setFunctionInKey k) . getFunctions

qualities :: [ChordQuality]
qualities = [ majorTriad
            , minorTriad
            , diminishedTriad
            , augmentedTriad
            , major7th
            , minor7th
            , dominant7th
            , halfDiminished7th
            , diminished7th
            , minormajor7th
            , major6th
            , minor6th ]
