{-# LANGUAGE BangPatterns #-}
module Omnear.Internal.Music.Voicing where

import Omnear.Internal.Music.Accidental
import Omnear.Internal.Music.Pitch
import Omnear.Internal.Music.Scale
import Omnear.Internal.Music.Interval
import Omnear.Internal.Music.Chord
import Omnear.Internal.Music.Transposable
import Omnear.Internal.AStar

import Data.Function (on)
import Data.List (minimumBy)
import Data.Ord (comparing)
import qualified Data.Set as S

type PitchGroup = [Pitch]

-- | A type alias for functions which evaluate a badness score for a group of
-- pitches.
type Badness = PitchGroup -> Double

-- | A type alias for functions which assign a voice leading badness for two
-- consecutive note clusters.
type LBadness = PitchGroup -> PitchGroup -> Double

-- | A type alias for functions which take a chord and return voicing options.
type VoiceOne = Chord -> [PitchGroup]

-- | A voicing is a function assigning a list of PitchGroups to a given Chord,
-- together with a function measuring the badness of one particular PitchGroup
-- and a function measuring the badness of the transition between two
-- PitchGroups.
data Voicing = Voicing VoiceOne Badness LBadness

voice :: Voicing -> Progression -> [PitchGroup]
voice (Voicing v b l) cs
          = fst <$> aStar all next init end bads badt heur
               where notesss = v <$> getChords cs
                     ln = length $ getChords cs
                     all = S.fromList . concat
                           $ zipWith zip notesss (repeat <$> [1..])
                     next (_, n) = (== (n + 1)) . snd
                     init = (== 1) . snd
                     end = (== ln) . snd
                     bads = b . fst
                     badt = l `on` fst
                     -- lb = minimum . fmap b . concat $ notesss
                     -- heur (ns, n) = minimum (l ns <$> last notesss)
                                      -- + lb * fromIntegral (ln - n)
                     lbs = fmap (minimum . fmap b) notesss
                     heur (_, n) = sum . drop n $ lbs

-- | Just to debug if `bruteForce` and `voice` yield different results.
findDiff xs ys = go 0 xs ys
  where go _ [] [] = Nothing
        go n (x:xs) [] = Just (Just x, Nothing, n)
        go n [] (y:ys) = Just (Nothing, Just y, n)
        go n (x:xs) (y:ys) | x == y = go (n+1) xs ys
                           | otherwise = Just (Just x, Just y, n)

bruteForce :: Voicing -> Progression -> [PitchGroup]
bruteForce (Voicing v b l) cs = minimumBy badness notesss
                where notesss = foldr (\nss lss -> (:) <$> nss <*> lss) [[]]
                                             $ v <$> getChords cs
                      badness
                        = comparing (\xss -> case xss of
                                 [] -> 0
                                 (zs : zss) -> (sum . fmap b $ xss)
                                      + snd (foldl (\(ys,n) xs
                                         -> (xs, l xs ys + n)) (zs, 0) zss))

-- | The sum of the numbers of semitones between corresponding Pitches between
-- two PitchGroups.
voiceLeadingBadness :: PitchGroup -> PitchGroup -> Double
voiceLeadingBadness ns ms =
    fromIntegral .  sum . map abs $ zipWith semitonesBetweenPitches ns ms


inversionOverBass :: Voicing
inversionOverBass = Voicing inversionOverBassOptions
                            inversionOverBassBadness
                            (((5 *) .) . voiceLeadingBadness)

inversionOverBassBadness :: Badness
inversionOverBassBadness ps = fromIntegral $ rootBadness + upperBadness
    where rootBadness  = abs $ semitonesBetweenPitches
                                    (P (O 4) (C Nt)) (head ps)
          upperBadness = sum $ map (semitonesBetweenPitches
                                    (P (O 5) (D Nt))) (tail ps)

inversionOverBassOptions :: VoiceOne
inversionOverBassOptions ch = withBass
    where withBass = inversions
                  >>= blockInRange (P (O 4) (A Nt), P (O 6) (C Nt))
                  >>= (\xs -> (:xs)
                              <$> inRange (P (O 3) (C Nt), P (O 4) (F Nt)) root)
          inversions = take (length upper) $ iterate (rotate 1) upper
          pcs = pitchClassesOfChord ch
          root = head pcs
          upper | length pcs <= 3 = pcs
                | otherwise       = drop (length pcs - 4) pcs

--data Voicing = CloseRoot         |
--               InversionOverBass |
--               CloseDoublingRoot |
--               OpenPart          |
--               Shell             |
--               LeftHand
--

-- | Returns all instances of the given PitchClass in the provided range.
inRange :: (Pitch, Pitch) -> PitchClass -> PitchGroup
inRange (n1, n2) p = let m1 = _getOctave $ p `above` n1
                         m2 = _getOctave $ p `below` n2
                     in  flip P p <$> [m1..m2]

-- | Takes a collection of PitchClasses and realizes it accross all octaves.
inAllOctaves :: [PitchClass] -> [PitchGroup]
inAllOctaves [] = []
inAllOctaves ps@(p : _) = runUp ps <$> as
                           where as = flip P p
                                      <$> interleave [O 0..] [O (-1), O (-2)..]

-- | Takes a collection of PitchClasses and returns all realizations in the
-- given range.
blockInRange :: (Pitch, Pitch) -> [PitchClass] -> [PitchGroup]
blockInRange _ [] = []
blockInRange (n1, n2) ps = takeWhile (all (<= n2))
                           . fmap (runUp ps) $ iterate (octave Ascending) n1

rotate :: Int -> [a] -> [a]
rotate _ [] = []
rotate n xs = zipWith const (drop n (cycle xs)) xs

--upperVoice :: Chord -> [PitchClass]
--upperVoice = pitchClassesOfChord
--
--topN = 4 :: Int
--
--voicingOptions :: Voicing -> Chord -> [NoteGroup]
--voicingOptions InversionOverBass ch =
--    take topN $ sortOn (voicingBadness InversionOverBass)
--              $ map NoteGroup withBass
--    where withBass = inversions
--                  >>= blockInRange (note A 0 4, note C 0 6)
--                  >>= (\xs -> (:xs) <$> inRange (note F 0 3, note F 0 4) root)
--          inversions = take (length upper) $ iterate (rotate 1) upper
--          pcs = pitchClassesOfChord ch
--          root = head pcs
--          upper | length pcs <= 3 = pcs
--                | length pcs <= 5 = tail pcs
--                | otherwise       = upperVoice ch
--
--voicingOptions v ch = take topN $ sortOn (voicingBadness v) pcs
--                            where pcs = inAllOctaves $ pitchClassesOfChord ch
--
--nextVoicingOptions :: Voicing -> NoteGroup -> Chord -> [NoteGroup]
--nextVoicingOptions v (NoteGroup []) = voicingOptions v
--nextVoicingOptions v ng = take topN . sortOn (voiceLeadingBadness ng)
--                                 . voicingOptions v
--
--
--voicingBadness :: Voicing -> NoteGroup -> Int
--voicingBadness InversionOverBass (NoteGroup ns) = rootBadness + upperBadness
--    where rootBadness  = abs $ semitonesBetween (note C 0 4) (head ns)
--          upperBadness = sum $ map (semitonesBetween (note D 0 5)) (tail ns)
--voicingBadness CloseRoot (NoteGroup ns) = sum $
--                                map (abs . semitonesBetween (note D 0 5)) ns
--voicingBadness _ ns = voicingBadness CloseRoot ns
--
--
--totalVoicingBadness :: Voicing -> [NoteGroup] -> Int
--totalVoicingBadness v = sum . map (voicingBadness v)
--
--
--
--totalVoiceleadingBadness :: [NoteGroup] -> Int
--totalVoiceleadingBadness [] = 0
--totalVoiceleadingBadness [n] = 0
--totalVoiceleadingBadness (n:m:ns) = voiceLeadingBadness n m
--                                    + totalVoiceleadingBadness (m:ns)
--
--
--totalBadness :: Voicing -> [NoteGroup] -> Int
--totalBadness v ns = totalVoicingBadness v ns + totalVoiceleadingBadness ns
--
--
--selectBestSequenceOf :: Voicing -> [[NoteGroup]] -> [NoteGroup]
--selectBestSequenceOf v = minimumBy (comparing $ totalBadness v)
--
--
--voiceChord :: Voicing -> Chord -> NoteGroup
--voiceChord v ch = minimumBy (comparing $ voicingBadness v) $ voicingOptions v ch
--
--
--voiceChords :: Voicing -> [Chord] -> [NoteGroup]
--voiceChords v chs = selectBestSequenceOf v options
--    where options         = foldr builder [] chs
--          builder ch []   = map (:[]) $ voicingOptions v ch
--          builder ch ngss = take 4 $ sortOn (totalBadness v)
--                                   $ ngss >>= brancher ch
--          brancher ch ngs =
--                    map (: ngs) (nextVoicingOptions v (head ngs) ch)
--
--
--
--
c = Chord   (C Nt) majorTriad
dm7 = Chord (D Nt) minor7th
e7 = Chord  (E Nt) dominant7th
f = Chord   (F Nt) majorTriad
g = Chord   (G Nt) majorTriad
g7 = Chord  (G Nt) dominant7th
testChords = [c,f,g,e7,dm7,g7,c]
