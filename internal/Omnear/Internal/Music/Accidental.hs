{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}

module Omnear.Internal.Music.Accidental  where

import Numeric.Natural

-- | The type of accidentals. Can express an arbitrary number of flats or sharps
-- (not mixed).
newtype Accidental = MkAccidental Int
  deriving (Eq, Ord)

{-# COMPLETE Fl, Nt, Sh #-}

-- | Pattern synonym for flats. `Fl 0` will give `Nt`.
pattern Fl :: Natural -> Accidental
pattern Fl a <- (\(MkAccidental x)
             -> if x<0 then Just (fromIntegral (abs x)) else Nothing -> Just a)
  where Fl a = MkAccidental (- fromIntegral a)

-- | Pattern synonym for natural.
pattern Nt :: Accidental
pattern Nt = MkAccidental 0

-- | Pattern synonym for sharps. `Sh 0` will give `Nt`.
pattern Sh :: Natural -> Accidental
pattern Sh a <- (\(MkAccidental x)
                   -> if x>0 then Just (fromIntegral x) else Nothing -> Just a)
  where Sh a = MkAccidental (fromIntegral a)

instance Show Accidental where
    show (Fl n) = replicate (fromIntegral n) 'b'
    show (Sh n) = replicate (fromIntegral n) '#'
    show Nt     = ""

instance Read Accidental where
    readsPrec _ s@('b':cs) = [(acc, rest)]
                where acc       = Fl . fromIntegral $ length bs
                      (bs, rest) = break (/= 'b') s
    readsPrec _ s@('#':cs) = [(acc, rest)]
                where acc       = Sh . fromIntegral $ length ss
                      (ss,rest) = break (/= '#') s
    readsPrec _ cs       = [(Nt, cs)]

class Pitchlike a where
    lower :: a -> a
    raise :: a -> a

instance Pitchlike Accidental where
    lower (Fl n)      = Fl (n + 1)
    lower Nt       = Fl 1
    lower (Sh 1)   = Nt
    lower (Sh n) = Sh (n - 1)

    raise (Sh n)    = Sh (n + 1)
    raise Nt      = Sh 1
    raise (Fl 1)   = Nt
    raise (Fl n) = Fl (n - 1)
