module Omnear.Internal.Music.Transposable  where

import Prelude hiding (transpose)

import Omnear.Internal.Music.Accidental
import Omnear.Internal.Music.Pitch
import Omnear.Internal.Music.Scale
import Omnear.Internal.Music.Interval

class Transposable a where
  semitoneUp    :: a -> a
  semitoneDown  :: a -> a
  transposeUp   :: Interval -> a -> a
  transposeDown :: Interval -> a -> a
  transpose     :: DInterval -> a -> a
  semitoneUp    = transpose (DI Ascending  (I (Aug 1) Unison))
  semitoneDown  = transpose (DI Descending (I (Aug 1) Unison))
  transposeUp   = transpose . DI Ascending
  transposeDown = transpose . DI Descending
  {-# MINIMAL transpose #-}

instance Transposable PitchClass where
  semitoneUp   = raise
  semitoneDown = lower
  transpose di@(DI d i@(I q n)) p =
    case d of
      Ascending
           | q `elem` [Maj, Prf]
               -> cycle (major `scaleOf` p) !! fromEnum n
           | q == Min
               -> cycle (phrygian `scaleOf` p) !! fromEnum n
           | isDiminished q
               -> transpose (DI d (augment i)) (lower p)
           | isAugmented q
               -> transpose (DI d (diminish i)) (raise p)
      Descending   -> transpose (invert di) p

octave :: Direction -> Pitch -> Pitch
octave Ascending  (P o n) = P (succ o) n
octave Descending (P o n) = P (pred o) n

interleave :: [a] -> [a] -> [a]
interleave [] ys = ys
interleave (x:xs) ys = x : interleave ys xs

instance Transposable Pitch where
  semitoneUp   = raise
  semitoneDown = lower
  transpose di@(DI d (I q s)) n@(P o p) = adjust
                                          $ whichway major n !! fromEnum s
        where adjust p' = let a = dIntervalToSemitones $ measureInterval n p'
                              b = dIntervalToSemitones di
                              f = if b > a then raise else lower
                              k = abs $ a - b
                          in iterate f p' !! k
              whichway  = if d == Descending then scaleDown else scaleUp
