module Omnear.Internal.Music.Scale where

import Data.List (unfoldr)

import Omnear.Internal.Music.Pitch

type Scale = [(Natural, Natural)]

major           :: Scale
major           = [(0,0),(2,1),(4,2),(5,3),(7,4),(9,5),(11,6)]

harmonicMinor   :: Scale
harmonicMinor   = [(0,0),(2,1),(3,2),(5,3),(7,4),(8,5),(11,6)]

melodicMinor    :: Scale
melodicMinor    = [(0,0),(2,1),(3,2),(5,3),(7,4),(9,5),(11,6)]

dorian          :: Scale
dorian          = [(0,0),(2,1),(3,2),(5,3),(7,4),(9,5),(10,6)]

phrygian        :: Scale
phrygian        = [(0,0),(1,1),(3,2),(5,3),(7,4),(8,5),(10,6)]

lydian          :: Scale
lydian          = [(0,0),(2,1),(4,2),(6,3),(7,4),(9,5),(11,6)]

mixolydian      :: Scale
mixolydian      = [(0,0),(2,1),(4,2),(5,3),(7,4),(9,5),(10,6)]

aeolian         :: Scale
aeolian         = [(0,0),(2,1),(3,2),(5,3),(7,4),(8,5),(10,6)]

locrian         :: Scale
locrian         = [(0,0),(1,1),(3,2),(5,3),(6,4),(8,5),(10,6)]

majorPentatonic :: Scale
majorPentatonic = [(0,0),(2,1),(4,2),(7,4),(9,5)]

minorPentatonic :: Scale
minorPentatonic = [(0,0),(3,2),(5,3),(7,4),(10,6)]

blues           :: Scale
blues           = [(0,0),(3,2),(5,3),(6,4),(7,4),(10,6)]

wholeTone       :: Scale
wholeTone       = [(0,0),(2,1),(4,2),(6,3),(8,4),(10,5)]

augmented       :: Scale
augmented       = [(0,0),(3,2),(4,2),(7,4),(8,4),(11,6)]

wholeHalfDim    :: Scale
wholeHalfDim    = [(0,0),(2,1),(3,2),(5,3),(6,3),(8,4),(9,5),(11,6)]

halfWholeDim    :: Scale
halfWholeDim    = [(0,0),(1,0),(3,1),(4,2),(6,3),(7,4),(9,5),(10,6)]

scaleOf :: Scale -> PitchClass -> [PitchClass]
sc `scaleOf` pc = map (uncurry $ semitonesAtDegree pc) sc

runUp :: [PitchClass] -> Pitch -> [Pitch]
runUp [] _ = []
runUp pcs p = unfoldr ( \(q, rs') -> case rs' of
                                  (r : rs) -> let s = r `above` q
                                              in  Just (s, (s, rs))
                                  []       -> Nothing ) (p, pcs)

runDown :: [PitchClass] -> Pitch -> [Pitch]
runDown pcs p = unfoldr ( \(q, rs') -> case rs' of
                                  (r : rs) -> let s = r `below` q
                                              in  Just (s, (s, rs))
                                  []       -> Nothing ) (p, pcs)

scaleUp :: Scale -> Pitch -> [Pitch]
sc `scaleUp` p = runUp (cycle $ sc `scaleOf` _getPitchClass p) p

scaleDown :: Scale -> Pitch -> [Pitch]
sc `scaleDown` p = runDown ( (_getPitchClass p :) . cycle . reverse
                             $ sc `scaleOf` _getPitchClass p ) p
