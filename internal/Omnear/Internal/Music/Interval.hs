{-# LANGUAGE ViewPatterns #-}
module Omnear.Internal.Music.Interval where

import Data.Function (on)
import Data.List (span)
import Data.Char (isDigit)
import Numeric.Natural

import Omnear.Internal.Music.Pitch
import Omnear.Internal.Music.Scale

class Invertible a where
  invert :: a -> a

data IntervalQuality = Prf
                     | Maj
                     | Min
                     | Aug Natural
                     | Dim Natural deriving Eq

instance Show IntervalQuality where
  show Prf     = "P"
  show Maj     = "M"
  show Min     = "m"
  show (Dim 1) = "dim"
  show (Dim n) = show n ++ "xd"
  show (Aug 1) = "Aug"
  show (Aug n) = show n ++ "xA"

instance Invertible IntervalQuality where
  invert i = case i of
       Prf    -> Prf
       Maj    -> Min
       Min    -> Maj
       Aug n  -> Dim n
       Dim n  -> Aug n

instance Read IntervalQuality where
    readsPrec _ s@(x:xs)
        | x == 'P'          = [(Prf, xs)]
        | x == 'M'          = [(Maj, xs)]
        | x == 'm'          = [(Min, xs)]
        | take 3 s == "Aug" = [(Aug 1, drop 3 s)]
        | take 3 s == "dim" = [(Dim 1, drop 3 s)]
        | otherwise         = try s
            where try s            = reads s >>= parsx
                  parsx (n,a:b:cs) = if n < 1
                                        then []
                                        else case [a,b] of
                                            "xd" -> [(Dim n,cs)]
                                            "xA" -> [(Aug n,cs)]
                                            _    -> []
                  parsx _          = []

isAugmented :: IntervalQuality -> Bool
isAugmented (Aug _) = True
isAugmented _       = False

isDiminished :: IntervalQuality -> Bool
isDiminished (Dim _) = True
isDiminished _       = False

data IntervalNum = Unison
                 | Second
                 | Third
                 | Fourth
                 | Fifth
                 | Sixth
                 | Seventh
                 | Octave
                 | Ninth
                 | Tenth
                 | Eleventh
                 | Twelfth
                 | Thirteenth
                 | Fourteenth
                 | Fifteenth deriving (Eq, Ord, Enum)

instance Show IntervalNum where
  show = show . (+ 1) . fromEnum

instance Read IntervalNum where
  readsPrec _ s = case span isDigit s of
                  ("", _) -> []
                  (ds, xs) -> if read ds > 15 || read ds < 1
                                  then []
                                  else [(toEnum (read ds - 1), xs)]

instance Invertible IntervalNum where
  invert i = case i of
         Unison     -> Octave
         Second     -> Seventh
         Third      -> Sixth
         Fourth     -> Fifth
         Fifth      -> Fourth
         Sixth      -> Third
         Seventh    -> Second
         Octave     -> Octave
         Ninth      -> Seventh
         Tenth      -> Sixth
         Eleventh   -> Fifth
         Twelfth    -> Fourth
         Thirteenth -> Third
         Fourteenth -> Second
         Fifteenth  -> Unison

isCompound :: IntervalNum -> Bool
isCompound = (> Octave)

perfectIntervals = [ Unison
                   , Fourth
                   , Fifth
                   , Octave
                   , Eleventh
                   , Twelfth
                   , Fifteenth ]

isPerfect :: IntervalNum -> Bool
isPerfect = (`elem` perfectIntervals)

data Direction = Ascending | Descending deriving Eq

instance Show Direction where
  show Ascending = "^"
  show Descending = "v"

instance Read Direction where
  readsPrec _ ('^' : cs) = [(Ascending , cs)]
  readsPrec _ ('v' : cs) = [(Descending, cs)]

instance Invertible Direction where
  invert Ascending  = Descending
  invert Descending = Ascending

data Interval = I { _getQuality   :: IntervalQuality
                  , _getName      :: IntervalNum } deriving Eq

instance Show Interval where
    show (I i n) = show i ++ show (fromEnum n + 1)

instance Read Interval where
  readsPrec _ cs = do (q, cs') <- reads cs
                      (n, cs'') <- reads cs'
                      pure (I q n, cs'')

instance Invertible Interval where
  invert (I q n) = I  (invert q) (invert n)

admissible         :: Interval -> Bool
admissible (I Prf     n) = isPerfect n
admissible (I Maj     n) = not . isPerfect $ n
admissible (I Min     n) = not . isPerfect $ n
admissible (I (Dim 0) n) = False
admissible (I (Aug 0) n) = False
admissible (I _       n) = True

mkInterval :: IntervalQuality -> IntervalNum -> Interval
mkInterval q n | admissible (I q n) = I q n
               | otherwise          = error "Ill formed interval"

intervalToSemitones :: Interval -> Int
intervalToSemitones i@(I q n) = case q of
                        Prf -> halfsteps n
                        Maj -> halfsteps n
                        Min -> halfsteps n - 1
                        Dim n -> intervalToSemitones (augment  i) - 1
                        Aug n -> intervalToSemitones (diminish i) + 1
        where halfsteps n = sum . take (fromEnum n) . cycle $ [2,2,1,2,2,2,1]

data DInterval = DI { _getDirection  :: Direction
                    , _getInterval   :: Interval } deriving Eq

instance Invertible DInterval where
  invert (DI d i) = DI (invert d) (invert i)

instance Show DInterval where
  show (DI d i) = symbol ++ show i
                          where symbol = case d of
                                   Ascending  -> "^"
                                   Descending -> "v"

instance Read DInterval where
  readsPrec _ cs = do (d, cs') <- reads cs
                      (i, cs'') <- reads cs'
                      pure (DI d i, cs'')

reverseDI :: DInterval -> DInterval
reverseDI (DI d i) = DI (invert d) i

dIntervalToSemitones :: DInterval -> Int
dIntervalToSemitones (DI d i) = case d of
                          Ascending  -> intervalToSemitones i
                          Descending -> - intervalToSemitones i

diminish :: Interval -> Interval
diminish (I (Aug 1) n) = if n `elem` perfectIntervals
                           then I Prf n
                           else I Maj n
diminish (I (Aug k) n) = I (Aug (k - 1)) n
diminish (I Maj     n) = I Min n
diminish (I Prf     n) = I (Dim 1) n
diminish (I Min     n) = I (Dim 1) n
diminish (I (Dim k) n) = I (Dim (k + 1)) n

augment :: Interval -> Interval
augment (I (Aug k) n) = I (Aug (k + 1)) n
augment (I Maj     n) = I (Aug 1) n
augment (I Prf     n) = I (Aug 1) n
augment (I Min     n) = I Maj n
augment (I (Dim 1) n) = if n `elem` perfectIntervals
                          then I Prf n
                          else I Min n
augment (I (Dim k) n) = I (Dim (k - 1)) n

measureInterval :: Pitch -> Pitch -> DInterval
measureInterval p r | p > r = (\(DI _ i) -> DI Descending i)
                                             $ measureInterval r p
measureInterval p r = DI Ascending i
     where (until, target:_) = span ((/= EQ) . pitchCompare' r)
                               $ major `scaleUp` p
           n                 = toEnum . reduce . length $ until
           reduce m          = if m > 14 then reduce (m - 7) else m
           i                 = iterate whichway ref !! num
           sts               = (subtract `on` pitchToMIDIKey) target r
           num               = abs sts
           whichway          = if sts > 0 then augment
                                          else diminish
           ref               = if n `elem` perfectIntervals then I Prf n
                                                            else I Maj n
