{-# LANGUAGE PatternSynonyms #-}
module Omnear.Internal.Music.Pitch ( module Omnear.Internal.Music.Pitch
                                   , module Numeric.Natural ) where

import Omnear.Internal.Music.Accidental

import Data.Function (on)
import Numeric.Natural ( Natural )
import Data.Bifunctor (first)

data Letter = C' | D' | E' | F' | G' | A' | B'
        deriving (Eq, Ord, Enum, Bounded)

instance Show Letter where
  show C' = "C"
  show D' = "D"
  show E' = "E"
  show F' = "F"
  show G' = "G"
  show A' = "A"
  show B' = "B"

instance Read Letter where
  readsPrec _ (x : xs) = case x of
            'C' -> [(C', xs)]
            'D' -> [(D', xs)]
            'E' -> [(E', xs)]
            'F' -> [(F', xs)]
            'G' -> [(G', xs)]
            'A' -> [(A', xs)]
            'B' -> [(B', xs)]

nextLetter :: Letter -> Letter
nextLetter B' = C'
nextLetter l = succ l

-- | Data type representing pitch classes.
data PitchClass = PC { _getLetter     :: Letter
                     , _getAccidental :: Accidental
                     } deriving (Eq, Ord)

-- | Pattern synonyms for more readable expression of `PitchClass`es.
pattern C a <- PC C' a
      where C a = PC C' a
pattern D a <- PC D' a
      where D a = PC D' a
pattern E a <- PC E' a
      where E a = PC E' a
pattern F a <- PC F' a
      where F a = PC F' a
pattern G a <- PC G' a
      where G a = PC G' a
pattern A a <- PC A' a
      where A a = PC A' a
pattern B a <- PC B' a
      where B a = PC B' a

instance Show PitchClass where
    show (PC l a) = show l ++ show a

instance Read PitchClass where
    readsPrec _ (x:xs) = case x of
        'C' -> first C <$> reads xs
        'D' -> first D <$> reads xs
        'E' -> first E <$> reads xs
        'F' -> first F <$> reads xs
        'G' -> first G <$> reads xs
        'A' -> first A <$> reads xs
        'B' -> first B <$> reads xs
        _   -> []
    readsPrec _ _ = []

instance Pitchlike PitchClass where
    lower (PC n a) = PC n $ lower a
    raise (PC n a) = PC n $ raise a

newtype Octave = O {getOctave :: Int} deriving (Eq, Ord)

instance Show Octave where
    show (O n) = show n

instance Read Octave where
    readsPrec n = fmap (first O) . readsPrec n

instance Enum Octave where
    toEnum = O
    fromEnum = getOctave

data Pitch = P { _getOctave     :: Octave
               , _getPitchClass :: PitchClass } deriving (Eq, Ord)

instance Show Pitch where
    show (P o pc) = show pc ++ show o

instance Read Pitch where
    readsPrec _ cs = reads cs >>= \(pc,str) -> map (\(o,str')
                                            -> (P o pc, str')) (reads str)
instance Pitchlike Pitch where
    lower (P o pc) = P o $ lower pc
    raise (P o pc) = P o $ raise pc

-- | A comparison for pitches which ignores the accidentals.
pitchCompare' :: Pitch -> Pitch -> Ordering
pitchCompare' = (compare `on` _getOctave)
                   <> (compare `on` _getLetter . _getPitchClass)

-- TODO: Type aliases Key and MKey
-- | Converts a given `Pitch` to midi key.
pitchToMIDIKey :: Pitch -> Int
pitchToMIDIKey (P o p) = 12 * (fromEnum o + 1) + semitones p
                where semitones (PC n a) = semin n + semia a
                      semin n = if n `elem` [C',D',E'] then 2 * fromEnum n
                                                       else 2 * fromEnum n - 1
                      semia (MkAccidental a) = a

-- | Returns the number of semitones between the given pitches. Ascending is
-- positive.
semitonesBetweenPitches :: Integral a => Pitch -> Pitch -> a
semitonesBetweenPitches p1 p2
            = fromIntegral $ on (-) pitchToMIDIKey p2 p1

-- | Returns the number of semitones between two PitchClasses, always counting
-- up from the first to the second.
semitonesBetweenPitchClasses :: Integral a => PitchClass -> PitchClass -> a
semitonesBetweenPitchClasses pc1 pc2
              = let diff = semitonesBetweenPitches (P (O 0) pc1) (P (O 0) pc2)
                in  diff `mod` 12

-- | @spellSemitonesAboveWithLetter pc n l@ moves up @n@ semitones from @pc@
-- and spells it with letter @l@.
--
-- >>> spellSemitonesAboveWithLetter (C Nt) 3 F'
-- Fbb
spellSemitonesAboveWithLetter :: PitchClass -> Natural -> Letter -> PitchClass
spellSemitonesAboveWithLetter p@(PC _ a@(MkAccidental m)) n l
            = PC l (MkAccidental acc)
                where p'  = PC l a
                      acc = (+ m) . subtract 6 . (`mod` 12) . (+ 6)
                              $ fromIntegral n
                                - semitonesBetweenPitchClasses p p'


semitonesAtDegree :: PitchClass -> Natural -> Natural -> PitchClass
semitonesAtDegree pc m n = spellSemitonesAboveWithLetter pc m $ name n
                     where name n = iterate nextLetter (_getLetter pc)
                                        !! fromIntegral n

--spellMIDIKeyWithLetter :: Int -> Letter -> Pitch
--spellMIDIKeyWithLetter n l = uncurry adjust
--                              . minimumBy (comparing (abs . snd)) $ keys
--            where keys = fmap (\p -> (p, ((-) n . pitchToMIDIKey) p))
--                            $ P <$> [O0 .. O10] <*> [PC l Nt]
--                  adjust q n | n == 0 = q
--                             | n > 0  = raise $ adjust q (n - 1)
--                             | n < 0  = lower $ adjust q (n + 1)

-- | Finds the first Pitch with a given PitchClass above the given Pitch, if it
-- exists.
above :: PitchClass -> Pitch -> Pitch
above c (P o pc) | c >= pc   = P o c
                 | otherwise = P (succ o) c

-- | Finds the first Pitch with a given PitchClass below the given Pitch, if it
-- exists.
below :: PitchClass -> Pitch -> Pitch
below c (P o pc) | c <= pc   = P o c
                 | otherwise = P (pred o) c
