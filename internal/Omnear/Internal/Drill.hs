{-# LANGUAGE DeriveGeneric #-}

module Omnear.Internal.Drill where

import Control.Monad
import Control.Monad.Trans.State.Lazy
import Data.Functor.Identity
-- import Control.Applicative (liftA2)
import Data.List (sortOn)
import GHC.Generics

import Test.QuickCheck (
                         Gen
                       , elements
                       -- , generate
                       -- , suchThat
                       )
import Data.Aeson

import Omnear.Internal.Music.Accidental
import Omnear.Internal.Music.Pitch
import Omnear.Internal.Music.Interval
import Omnear.Internal.Music.Chord
-- import Omnear.Internal.Music.Scale
import Omnear.Internal.Music.Voicing
import Omnear.Internal.Music.Transposable

data Syllable = Do
              | Di
              | Ra
              | Re
              | Ri
              | Me
              | Mi
              | Fa
              | Fi
              | Se
              | So
              | Si
              | Le
              | La
              | Li
              | Te
              | Ti deriving (Show, Generic)

toDegree :: Syllable -> Degree
toDegree Do = I Maj     Unison
toDegree Di = I (Aug 1) Unison
toDegree Ra = I Min     Second
toDegree Re = I Maj     Second
toDegree Ri = I (Aug 1) Second
toDegree Me = I Min     Third
toDegree Mi = I Maj     Third
toDegree Fa = I Maj     Fourth
toDegree Fi = I (Aug 1) Fourth
toDegree Se = I (Dim 1) Fifth
toDegree So = I Prf     Fifth
toDegree Si = I (Aug 1) Fifth
toDegree Le = I Min     Sixth
toDegree La = I Maj     Sixth
toDegree Li = I (Aug 1) Sixth
toDegree Te = I Min     Seventh
toDegree Ti = I Maj     Seventh

data Prompt = CircularSyllables { choices :: [Syllable]
                                , answer  :: [Syllable] }
                                      deriving (Show, Generic)

type Tempo = Int
type Tick = Int
type Key = Int
type Velocity = Int
type TicksPerBeat = Int

data Event = NoteOn  { key :: Key, velocity :: Velocity }
           | NoteOff { key :: Key, velocity :: Velocity }
                                          deriving (Show, Generic)

data Message = Message { tick  :: Tick
                       , event :: Event } deriving (Show, Generic)

data Midi = Midi { tempo    :: Tempo
                 , tpb      :: TicksPerBeat
                 , messages :: [Message] } deriving (Show, Generic)

data Block = MIDIBlock Midi
           | PromptBlock Prompt
           | AudioBlock FilePath
           | Seq [Block]
           | Sim [Block]
           | Loop Block deriving (Show, Generic)

instance ToJSON Event
instance ToJSON Prompt
instance ToJSON Block
instance ToJSON Midi
instance ToJSON Message
instance ToJSON Syllable

pitchFromToTick :: Functor f => Tick
                             -> Tick
                             -> f Pitch
                             -> f [Message]
pitchFromToTick t0 t1 = fmap ( \p
                          -> [ Message t0 (NoteOn  (pitchToMIDIKey p) 100)
                             , Message t1 (NoteOff (pitchToMIDIKey p) 100) ] )

pitchFromToTick' :: Tick -> Tick -> Pitch -> [Message]
pitchFromToTick' t0 t1 p = runIdentity . pitchFromToTick t0 t1 $ Identity p

measure :: [Pitch] -> State Int [Message]
measure ps = concat <$> state (\s -> ( pitchFromToTick s (s + 3) ps , s + 4))

randomDegreesFromKey :: PitchClass -> [Degree] -> Gen Pitch
randomDegreesFromKey k ds = do
                      d <- elements ds
                      o <- elements [O 3 .. O 7]
                      pure $ P o (transposeUp d k)

randomDegreesFromKeyBlock :: [Int] -> PitchClass -> [Degree] -> Gen Block
randomDegreesFromKeyBlock ns k ds = do
                    let r = randomDegreesFromKey k ds
                    pss <- traverse (`replicateM` r) ns
                    pure . MIDIBlock . Midi 100 1 . sortOn tick . concat
                         $ evalState (traverse (placeForTicks 4) pss) 0

placeForTicks :: Tick -> [Pitch] -> State Int [Message]
placeForTicks n ps = state $ \s -> (concat $ pitchFromToTick s (s + n) ps, s + n)

cMajorI_IV_V_I :: Gen Block
cMajorI_IV_V_I = let pss = voice inversionOverBass
                                  $ setCadenceInKey (C Nt)
                                    major_I_IV_V_I_triad
                  in pure . MIDIBlock . Midi 100 1
                                . sortOn tick . concat
                                $ evalState (traverse measure pss) 0

exercise1 :: Gen Block
exercise1 = do s <- elements [Do, So]
               Seq <$> sequence [ cMajorI_IV_V_I
                                , pure . PromptBlock
                                          $ CircularSyllables [Do, So] [s]
                                , randomDegreesFromKeyBlock [1]
                                            (C Nt)
                                                  (toDegree <$> [s]) ]

majorKeyCenters :: [PitchClass]
majorKeyCenters = ([C Nt, F Nt] ++)
                        $ (flip ($)) <$> [Fl 1, Nt] <*> [B, E, A, D, G]

randomMajorKeyCenter :: Gen PitchClass
randomMajorKeyCenter = elements majorKeyCenters

randomMajorI_IV_V_I :: Gen Block
randomMajorI_IV_V_I = do k <- randomMajorKeyCenter
                         let pss = voice inversionOverBass
                                      $ setCadenceInKey k major_I_IV_V_I_triad
                         pure $ MIDIBlock . Midi 100 1 . sortOn tick . concat
                              $ evalState (traverse measure pss) 0

--data Drill = Drill { getType      :: Type
--                   , getKeys      :: Keys
--                   , getPolyphony :: Polyphony
--                   , getRange     :: Range
--                   , getDegrees   :: Degrees
--                   , getContext   :: Context
--                   , getTempo     :: Tempo }
--
--testDrill = Drill Recognize
--            allMajorKeys
--            [1]
--            (note C 0 3, note C 0 7)
--            allDegrees
--            (Chords [major_I_IV_V_I_triad] [InversionOverBass])
--            (BPM 150)
--
--data Type = Recognize | Reproduce deriving Eq
--data Prompt = Prompt [Int] Range [HInterval]
--data Context = Chords [Cadence] [Voicing]
--data Result = Result
--
--type Polyphony = [Int]
--type Degrees = [Degree]
--type Range  = (Note, Note)
--type Keys = [Key]
--
--pickDegrees :: [HInterval] -> [Int] -> IO [[HInterval]]
--pickDegrees is = mapM f
--        where f n = mapM (const (generate . elements $ is)) [1..n]
--
--generatePrompt :: Polyphony
--               -> Degrees
--               -> Range
--               -> Key
--               -> IO (Part, [[HInterval]])
--generatePrompt ns is r key = do
--       iss <- pickDegrees is ns
--       pss <- pure . (map . map) (flip transpose (tonic key)
--                                . convertHIntToMInt Ascending) $ iss
--       nsss <- pure . (map . map) (inRange r) $ pss
--       nss <- (mapM . mapM) (generate . elements) nsss
--       zips <- pure $ map (sortOn fst) $ zipWith zip nss iss
--       part <- pure . Part . map (Verticality
--                                 . (,2) . NoteGroup . map fst) $ zips
--       pure (part, map (map snd) zips)
--
--displayAnswer :: Type -> [[HInterval]] -> IO ()
--displayAnswer _ = print
--
--wholeRest = Verticality (NoteGroup [],1)
--
--generateContext :: Context -> Key -> IO Part
--generateContext (Chords cs vs) key = do
--                cadence    <- generate . elements $ cs
--                voicing    <- generate . elements $ vs
--                let noteGroups = voiceChords voicing $
--                        map (setFunctionInKey (tonic key)) cadence
--                pure $ Part . map Verticality $
--                       zip (noteGroups ++ [NoteGroup []]) $ repeat 2
--
--playDrill :: FS -> Drill -> IO Result
--playDrill fs d  = do
--              let dType = getType d
--                  dDegrees = getDegrees d
--                  dTempo = getTempo d
--                  dRange = getRange d
--                  dPolyphony = getPolyphony d
--              key <- generate . elements $ getKeys d
--              (prompt, answer) <- generatePrompt dPolyphony dDegrees dRange key
--              context <- generateContext (getContext d) key
--              play context dTempo fs
--              let phases = [ displayAnswer dType answer
--                           , play wholeRest dTempo fs
--                           , play prompt dTempo fs ]
--              if dType == Reproduce then sequence_ phases
--                                    else sequence_ $ reverse phases
--              pure Result
--
--
--runDrill :: Drill -> IO ()
--runDrill d = do
--        fs <- initFS defaultInit
--        forever $ playDrill fs d
