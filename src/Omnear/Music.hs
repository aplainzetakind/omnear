{-# LANGUAGE PatternSynonyms #-}
module Omnear.Music (module Omnear.Music) where

import Omnear.Internal.Music.Accidental as Omnear.Music ( Accidental
                                                        , Pitchlike (..)
                                                        , pattern Fl
                                                        , pattern Nt
                                                        , pattern Sh )
import Omnear.Internal.Music.Pitch as Omnear.Music ( Pitch (..)
                                                   , PitchClass
                                                   , pitchCompare'
                                                   , pattern C
                                                   , pattern D
                                                   , pattern E
                                                   , pattern F
                                                   , pattern G
                                                   , pattern A
                                                   , pattern B
                                                   , semitonesBetweenPitches
                                                   , semitonesAtDegree
                                                   , pitchToMIDIKey
                                                   , above
                                                   , below
                                                   , Octave (..) )
import Omnear.Internal.Music.Scale as Omnear.Music ( Scale (..)
                                                    , scaleOf
                                                    , runUp
                                                    , runDown )
import Omnear.Internal.Music.Interval as Omnear.Music
import Omnear.Internal.Music.Transposable as Omnear.Music
import Omnear.Internal.Music.Chord as Omnear.Music
import Omnear.Internal.Music.Voicing as Omnear.Music
