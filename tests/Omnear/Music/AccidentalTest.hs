{-# LANGUAGE LambdaCase #-}
module Omnear.Music.AccidentalTest where

import Omnear.Internal.Music.Accidental

import Test.Tasty
import Test.Tasty.QuickCheck as QC

instance Arbitrary Accidental where
  arbitrary = MkAccidental <$> arbitrary

qcAccidental :: TestTree
qcAccidental = testGroup "Tests for Accidental"
  [ QC.testProperty "read . show = id"
                  $ \acc -> (read . show) acc == (acc :: Accidental)
  , QC.testProperty "raise . lower = id"
                  $ \acc -> (raise . lower) acc == (acc :: Accidental)
  , QC.testProperty "lower . raise = id"
                  $ \acc -> (lower . raise) acc == (acc :: Accidental)
  , QC.testProperty "Correct number of steps to Nt"
                  $ \case (Fl n) -> iterate raise (Fl n)
                                             !! fromIntegral n == Nt
                          (Sh n) -> iterate lower (Sh n)
                                             !! fromIntegral n     == Nt
                          Nt     -> True
  ]
