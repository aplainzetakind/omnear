module Omnear.Music.ScaleTest where

import Omnear.Internal.Music.Scale

import Test.Tasty
import Test.Tasty.HUnit

scale = testGroup "Scale" [uScale]

uScale = testGroup "Unit tests for scales"
  [ testCase "C Major" $
      major `scaleOf` read "C"
                     @?= read <$> words "C D E F G A B"
  , testCase "F harmonic minor" $
      harmonicMinor `scaleOf` read "F"
                     @?= read <$> words "F G Ab Bb C Db E"
  , testCase "F melodic minor" $
      melodicMinor `scaleOf` read "F"
                     @?= read <$> words "F G Ab Bb C D E"
  , testCase "Db Dorian" $
      dorian `scaleOf` read "Db"
                     @?= read <$> words "Db Eb Fb Gb Ab Bb Cb"
  , testCase "C Phrygian" $
      phrygian `scaleOf` read "C"
                     @?= read <$> words "C Db Eb F G Ab Bb"
  , testCase "A# Lydian" $
      lydian `scaleOf` read "A#"
                     @?= read <$> words "A# B# C## D## E# F## G##"
  , testCase "Cb Mixolydian" $
      mixolydian `scaleOf` read "Cb"
                     @?= read <$> words "Cb Db Eb Fb Gb Ab Bbb"
  , testCase "Gb Aeolian" $
      aeolian `scaleOf` read "G#"
                     @?= read <$> words "G# A# B C# D# E F#"
  , testCase "Ab Locrian" $
      locrian `scaleOf` read "Ab"
                     @?= read <$> words "Ab Bbb Cb Db Ebb Fb Gb"
  , testCase "D# major pentatonic" $
      majorPentatonic `scaleOf` read "D#"
                     @?= read <$> words "D# E# F## A# B#"
  , testCase "E# minor pentatonic" $
      minorPentatonic `scaleOf` read "E#"
                     @?= read <$> words "E# G# A# B# D#"
  , testCase "B Blues" $
      blues `scaleOf` read "B"
                     @?= read <$> words "B D E F F# A"
  , testCase "Db whole tone" $
      wholeTone `scaleOf` read "Db"
                     @?= read <$> words "Db Eb F G A B"
  , testCase "C augmented" $
      augmented `scaleOf` read "C"
                     @?= read <$> words "C Eb E G G# B"
  , testCase "C whole-half diminished" $
      wholeHalfDim `scaleOf` read "C"
                     @?= read <$> words "C D Eb F F# G# A B"
  , testCase "C half-whole diminished" $
      halfWholeDim `scaleOf` read "C"
                     @?= read <$> words "C C# D# E F# G A Bb"
  ]
