module Omnear.Music.PitchTest where

import Omnear.Internal.Music.Pitch
import Omnear.Music.AccidentalTest ()

import Test.Tasty
import Test.Tasty.QuickCheck as QC

instance Arbitrary Natural where
  arbitrary = fromIntegral <$> (arbitrary :: Gen Int) `suchThat` (>= 0)

instance Arbitrary Letter where
  arbitrary = QC.elements [C' .. B']

instance Arbitrary PitchClass where
  arbitrary = PC <$> arbitrary <*> arbitrary

instance Arbitrary Octave where
  arbitrary = O <$> arbitrary

instance Arbitrary Pitch where
  arbitrary = P <$> arbitrary <*> arbitrary

-- C 0 -> MIDI 12
qcPitch :: TestTree
qcPitch = testGroup "Tests for Pitch"
  [ QC.testProperty "Letter: read . show = id"
                  $ \ltr -> (read . show) ltr == (ltr :: Letter)
  , QC.testProperty "PitchClass: read . show = id"
                  $ \pc  -> (read . show) pc  == (pc :: PitchClass)
  , QC.testProperty "Octave: read . show = id"
                  $ \o   -> (read . show) o   == (o :: Octave)
  , QC.testProperty "Pitch: read . show = id"
                  $ \p   -> (read . show) p   == (p :: Pitch)
  , QC.testProperty "above is above"
                  $ \(pc, p) -> (>= p) $ pc `above` p
  , QC.testProperty "below is below"
                  $ \(pc, p) -> (<= p) $ pc `below` p
  , QC.testProperty "spellSemitonesAboveWithLetter is sane"
                  $ \(pc, n, l) -> semitonesBetweenPitchClasses pc
                                     (spellSemitonesAboveWithLetter pc n l)
                                        - fromIntegral n `mod` 12 == 0
  , QC.testProperty "semitonesBetween[Pitches/PitchClasses] are compatible"
                  $ \(p@(P o cl1), cl2)
                       -> let p' = cl2 `above` p
                          in semitonesBetweenPitchClasses cl1 cl2
                             - semitonesBetweenPitches p p' `mod` 12 == 0
  , QC.testProperty "semitonesBetweenPitchClasses is nonnegative"
                  $ \(pc1, pc2) -> semitonesBetweenPitchClasses pc1 pc2 >= 0
  , QC.testProperty "semitonesAtDegree does not crash"
                  $ \(pc, m, n) -> seq (semitonesAtDegree pc m n) True
  , QC.testProperty "semitonesAtDegree is sane."
                  $ \(pc, m, n) -> semitonesBetweenPitchClasses pc
                                      (semitonesAtDegree pc m n)
                                         - fromIntegral m  `mod` 12 == 0 ]
