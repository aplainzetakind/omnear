module Omnear.Music.TransposableTest where

import Omnear.Internal.Music.Interval
import Omnear.Internal.Music.Pitch
import Omnear.Internal.Music.Transposable
import Omnear.Music.AccidentalTest ()
import Omnear.Music.PitchTest ()
import Omnear.Music.IntervalTest ()

import Test.Tasty
import Test.Tasty.QuickCheck as QC

normalize :: DInterval -> DInterval
normalize (DI Ascending  (I (Dim n) Unison)) = DI Descending (I (Aug n) Unison)
normalize (DI Descending (I (Dim n) Unison)) = DI Ascending  (I (Aug n) Unison)
normalize (DI Descending (I Prf Unison)) = DI Ascending  (I Prf Unison)
normalize di = di

transposable :: TestTree
transposable = testGroup "Tests for Transposable"
  [ QC.testProperty "measureInterval p (transpose di p) == di"
                  $ \p di -> let di' = normalize di
                             in measureInterval p (transpose di p) == di'
  , QC.testProperty "transpose di . transpose (reverseDI di) == id"
                  $ \pc di -> (transpose di . transpose (reverseDI di)) pc
                                   == (pc :: PitchClass)
  , QC.testProperty
           "transpose di . transpose di' == transpose di' . transpose di"
                  $ \pc di di' -> (transpose di . transpose di') pc
                                   == (transpose di . transpose di')
                                        (pc :: PitchClass)
  ]
