module Omnear.Music.IntervalTest where

import Omnear.Internal.Music.Interval
import Omnear.Internal.Music.Pitch
import Omnear.Music.AccidentalTest ()
import Omnear.Music.PitchTest ()

import Test.Tasty
import Test.Tasty.QuickCheck as QC

instance Arbitrary IntervalQuality where
  arbitrary = QC.oneof [pure Prf, pure Maj, pure Min, arbdim, arbaug]
                where arbdim = Dim <$> QC.suchThat arbitrary (> 0)
                      arbaug = Aug <$> QC.suchThat arbitrary (> 0)

instance Arbitrary IntervalNum where
  arbitrary = elements [Unison .. Fifteenth]

instance Arbitrary Direction where
  arbitrary = elements [Ascending, Descending]

instance Arbitrary Interval where
  arbitrary = suchThat (I <$> arbitrary <*> arbitrary) admissible

instance Arbitrary DInterval where
  arbitrary = DI <$> arbitrary <*> arbitrary

-- C 0 -> MIDI 12
interval :: TestTree
interval = testGroup "Tests for Pitch"
  [ QC.testProperty "IntervalQuality: read . show = id"
                  $ \iq   -> (read . show) iq  == (iq :: Letter)
  , QC.testProperty "IntervalNum: read . show = id"
                  $ \inm  -> (read . show) inm == (inm :: IntervalNum)
  , QC.testProperty "Direction: read . show = id"
                  $ \d    -> (read . show) d   == (d :: Direction)
  , QC.testProperty "Interval: read . show = id"
                  $ \i    -> (read . show) i   == (i :: Interval)
  , QC.testProperty "DInterval: read . show = id"
                  $ \di   -> (read . show) di  == (di :: DInterval)
  , QC.testProperty "diminish . augment = id"
                  $ \i   -> (diminish . augment) i  == (i :: Interval)
  , QC.testProperty "augment . diminish = id"
                  $ \i   -> (augment . diminish) i  == (i :: Interval)
  ]
