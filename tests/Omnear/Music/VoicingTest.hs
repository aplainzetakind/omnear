module Omnear.Music.VoicingTest where

import Omnear.Internal.Music.Voicing
import Omnear.Music.AccidentalTest ()
import Omnear.Music.PitchTest ()
import Omnear.Music.IntervalTest ()
import Omnear.Music.ChordTest ()

import Test.Tasty
import Test.Tasty.QuickCheck as QC

voicingTests :: TestTree
voicingTests = testGroup "Tests for Voicing"
  [ QC.testProperty "Just 100 runs of voice"
       $ \p -> voice inversionOverBass p `seq` True
  , QC.testProperty "Brute forcing and AStar yield the same result"
       $ \p -> voice inversionOverBass p == bruteForce inversionOverBass p
  ]
