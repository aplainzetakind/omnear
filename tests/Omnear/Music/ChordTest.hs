module Omnear.Music.ChordTest where

import Omnear.Internal.Music.Transposable
import Omnear.Internal.Music.Chord
import Omnear.Music.AccidentalTest ()
import Omnear.Music.PitchTest ()
import Omnear.Music.IntervalTest ()

import Test.Tasty
import Test.Tasty.QuickCheck as QC

instance Arbitrary ChordQuality where
  arbitrary =  QC.elements qualities

instance Arbitrary Chord where
  arbitrary = Chord <$> arbitrary <*> arbitrary

instance Arbitrary Progression where
  arbitrary = fmap Prog . oneof $ flip vectorOf arbitrary <$> [4..8]

chord :: TestTree
chord = testGroup "Tests for Chord"
  [ QC.testProperty "transposing and building a chord commute"
       $ \p cq di -> fmap (transpose di) (pitchClassesOfChord (Chord p cq))
                     == pitchClassesOfChord (Chord (transpose di p) cq)
  ]
