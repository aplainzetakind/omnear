module Main where

import Omnear.Music.AccidentalTest
import Omnear.Music.PitchTest
import Omnear.Music.ScaleTest
import Omnear.Music.IntervalTest
import Omnear.Music.TransposableTest
import Omnear.Music.ChordTest
import Omnear.Music.VoicingTest

import Test.Tasty

tests = testGroup "Tests"
          [ qcAccidental
          , qcPitch
          , scale
          , interval
          , transposable
          , chord
          , voicingTests ]

main :: IO ()
main = defaultMain tests
